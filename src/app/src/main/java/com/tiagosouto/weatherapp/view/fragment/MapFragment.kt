package com.tiagosouto.weatherapp.view.fragment


import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tiagosouto.weatherapp.R
import com.tiagosouto.weatherapp.core.util.PermissionUtil
import com.tiagosouto.weatherapp.view.activity.MainActivity
import com.tiagosouto.weatherapp.view.adapter.MapInfoAdapter


/**
 * A simple [Fragment] subclass.
 * Use the [MapFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MapFragment : Fragment(), OnMapReadyCallback {

    private val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"

    private var mView: View? = null
    private var mMap: GoogleMap? = null
    private var mapView: MapView? = null
    private var mapViewBundle: Bundle? = null
    private var motiveMove: Int = GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        this.mView = inflater.inflate(R.layout.fragment_map, container, false)
        this.init()
        return this.mView
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        var mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY)
        if (mapViewBundle == null) {
            mapViewBundle = Bundle()
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle)
        }

        mapView!!.onSaveInstanceState(mapViewBundle)
    }

    override fun onResume() {
        super.onResume()
        mapView!!.onResume()

//        this.viewModel = ViewModelProviders.of(this, WeatherViewModelFactory(this.activity!!)).get(WeatherViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        mapView!!.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView!!.onStop()
    }

    override fun onPause() {
        mapView!!.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mapView!!.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView!!.onLowMemory()
    }

    private fun init () {
        mapView = mView!!.findViewById(R.id.map)
        mapView!!.onCreate(mapViewBundle)
        mapView!!.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val adapter = MapInfoAdapter(this.activity!!)
        mMap!!.setInfoWindowAdapter(adapter)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PermissionUtil.hasPermission(this.context!!, PermissionUtil.ACCESS_FINE_LOCATION)) {
                mMap!!.isMyLocationEnabled = true
            }
        }

        val act = this.activity!! as MainActivity
        act.viewModel.getWeather().observe(this, Observer {
            if (it != null && !it.isEmpty()) {
                mMap!!.clear()
                adapter.addList(it)
                for ((key, item) in it.withIndex()) {
                    val point = LatLng(item.coord.latitude, item.coord.longitude)
                    mMap!!.addMarker(MarkerOptions().position(point).title(item.name).flat(true).zIndex(key.toFloat()))
                }
            }
        })

        val point = LatLng(act.latitude, act.longitude)
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 9f))

        mMap!!.setOnCameraMoveStartedListener {
            motiveMove = it
        }

        mMap!!.setOnCameraIdleListener {
            if (motiveMove == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                val stopPoint = mMap!!.cameraPosition.target
                act.viewModel.getWeather(stopPoint.latitude, stopPoint.longitude)
            }
        }
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment MapFragment.
         */
        @JvmStatic
        fun newInstance() = MapFragment()
    }
}
