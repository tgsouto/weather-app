package com.tiagosouto.weatherapp.core.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.tiagosouto.weatherapp.App
import com.tiagosouto.weatherapp.core.util.ExceptionUtil
import com.tiagosouto.weatherapp.core.util.ViewModelUtil
import com.tiagosouto.weatherapp.data.model.RequestModel
import com.tiagosouto.weatherapp.data.model.ResponseModel
import com.tiagosouto.weatherapp.data.repository.WeatherRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class WeatherViewModel (private var context: Context, private var weatherRepository: WeatherRepository): ViewModel() {

    private var weatherData = MutableLiveData<List<ResponseModel.WeatherObject>>()
    private var weather: LiveData<List<ResponseModel.WeatherObject>>
    private val appid: String
    private val appPrefs = App.getAppPrefs()

    var loading = MutableLiveData<Boolean>()
    init {
        this.loading.value = false
        this.appid = App.apiId

        this.weather = Transformations.map(weatherData) {
            this.loading.value = false
            it
        }
    }

    fun getWeather (latitude: Double, longitude: Double) {
        this.loading.value = true
        this.loadWeather(latitude, longitude)
    }

    fun getWeather (): LiveData<List<ResponseModel.WeatherObject>> {
        return this.weather
    }

    private fun loadWeather (latitude: Double, longitude: Double) {
        val weatherModel = RequestModel.Weather(latitude, longitude, this.appid, ViewModelUtil.getMeasurement(appPrefs.measurement))

        this.weatherRepository.getWeather(weatherModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    val newList = it.list.distinctBy { it.name }

                    for (item in newList) {
                        item.distance = ViewModelUtil.calcDistance(weatherModel, item.coord)
                    }

                    weatherData.value = newList
                            .filter { it.distance <= 50.0}
                            .sortedBy { it.distance }
                }, { err ->
                    err.printStackTrace()
                    val message = ExceptionUtil.getMessage(context, err)
                    if(message.isNotEmpty()){
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                    }

                })
    }
}