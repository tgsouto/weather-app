package com.tiagosouto.weatherapp.view.activity

import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import com.tiagosouto.weatherapp.R
import com.tiagosouto.weatherapp.core.util.PermissionUtil
import kotlinx.android.synthetic.main.activity_request_permission.*

class RequestPermissionActivity : BaseActivity() {
    private val REQUEST_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_permission)

        btnGivePermission.setOnClickListener {
            loading.visibility = View.VISIBLE
            btnGivePermission.visibility = View.GONE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val permissions = arrayOf(PermissionUtil.ACCESS_FINE_LOCATION)
                requestPermissions(permissions, REQUEST_CODE)
            } else {
                this.redirToMain()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.redirToMain()
                } else {
                    loading.visibility = View.GONE
                    btnGivePermission.visibility = View.VISIBLE
                }
            }
        }
    }
}
