package com.tiagosouto.weatherapp.data.repository

import com.tiagosouto.weatherapp.data.model.RequestModel
import com.tiagosouto.weatherapp.data.model.ResponseModel
import com.tiagosouto.weatherapp.data.remote.API
import io.reactivex.Observable

class WeatherRepository (private var api: API) {

    fun getWeather (weather: RequestModel.Weather): Observable<ResponseModel.WeatherResponse> {
        return api.weather(weather.toMap())
    }

}