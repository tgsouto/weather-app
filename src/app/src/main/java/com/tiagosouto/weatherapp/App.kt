package com.tiagosouto.weatherapp

import android.app.Application
import android.content.pm.PackageManager
import com.tiagosouto.weatherapp.data.local.AppPrefs

class App: Application() {

    companion object {
        const val urlIcon = "http://openweathermap.org/img/w/"
        private lateinit var _appPrefs: AppPrefs
        lateinit var apiId: String
        fun getAppPrefs() = _appPrefs
    }

    override fun onCreate() {
        super.onCreate()
        _appPrefs = AppPrefs(this)
        apiId = this.packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA).metaData["APPID"].toString()
    }
}