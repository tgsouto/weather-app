package com.tiagosouto.weatherapp.data.local

import android.content.Context
import android.content.SharedPreferences
import com.tiagosouto.weatherapp.R

class AppPrefs(context: Context) {
    private var sharedPreferences: SharedPreferences = context.getSharedPreferences("AppPrefs", Context.MODE_PRIVATE)

    var measurement: Int
        get() = this.sharedPreferences.getInt("MEASUREMENT", R.id.menu_celsius)
        set(value) {
            this.sharedPreferences.edit().putInt("MEASUREMENT", value).apply()
        }

    var measurementName: String
        get() = this.sharedPreferences.getString("MEASUREMENT_NAME", "°C")
        set(value) {
            this.sharedPreferences.edit().putString("MEASUREMENT_NAME", value).apply()
        }

}