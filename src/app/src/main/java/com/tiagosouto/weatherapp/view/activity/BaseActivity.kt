package com.tiagosouto.weatherapp.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    override fun onResume() {
        super.onResume()
        if (!this.hasConnection()) {
            this.redirToNoConnection()
        }
    }

    protected fun hasConnection (): Boolean {
        val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    protected fun redirToNoConnection () {
        val intent = Intent(applicationContext, NoConnectionActivity::class.java)
        this.startActivity(intent)
        finish()
    }

    protected fun redirToMain () {
        val intent = Intent(applicationContext, MainActivity::class.java)
        this.startActivity(intent)
        finish()
    }

    protected fun redirToRequestPermission () {
        val intent = Intent(applicationContext, RequestPermissionActivity::class.java)
        this.startActivity(intent)
        finish()
    }
}