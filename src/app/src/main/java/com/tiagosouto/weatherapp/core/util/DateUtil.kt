package com.tiagosouto.weatherapp.core.util

import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    companion object {

        private var format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

        fun stringToCalendar(string: String) : Calendar {
            val cal = Calendar.getInstance()
            cal.time = format.parse(string)
            return cal
        }

        fun dateToCalendar(date: Date) : Calendar {
            val cal = Calendar.getInstance()
            cal.time = date
            return cal
        }

        fun dateToString (cal: Calendar, pattern: String = "") : String {
            val p = if (pattern == "") format else pattern
            return SimpleDateFormat(p.toString(), Locale.getDefault()).format(cal.time)
        }

        fun formatDate (date: Date, pattern: String = "") : String {
            val p = if (pattern == "") format else pattern
            return SimpleDateFormat(p.toString(), Locale.getDefault()).format(date)
        }

        fun formatDate (date: Long, pattern: String) : String {
            return SimpleDateFormat(pattern, Locale.getDefault()).format(date)
        }

        fun formatAtNow (cal: Calendar) : String {
            val now = Calendar.getInstance()
            val format: String
            if (cal[Calendar.DATE + Calendar.MONTH + Calendar.YEAR] == now[Calendar.DATE + Calendar.MONTH + Calendar.YEAR]) {
                format = dateToString(cal, "HH:mm")
            } else {
                format = dateToString(cal, "dd MMM")
            }

            return format
            //return DateUtils.getRelativeTimeSpanString(cal.timeInMillis, now.timeInMillis, DateUtils.HOUR_IN_MILLIS).toString()
        }

        fun dateNow () : Date {
            val cal = Calendar.getInstance()
            return cal.time
        }
    }
}