package com.tiagosouto.weatherapp.data.remote

import com.tiagosouto.weatherapp.data.model.ResponseModel
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface API {

    companion object {
        fun create(): API {
            val baseUrl = "http://api.openweathermap.org/data/2.5/"
            val retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            return retrofit.create(API::class.java)
        }
    }

    @GET("find")
    fun weather(@QueryMap options: Map<String, String>): Observable<ResponseModel.WeatherResponse>
}