package com.tiagosouto.weatherapp.core.util

import android.content.Context
import com.tiagosouto.weatherapp.R
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

class ExceptionUtil {
    companion object {
        fun getMessage(context: Context, err: Throwable): String {
            when(err){
                is HttpException -> {
                    var responseBody = err.response().errorBody()
                    return responseBody?.string() ?: ""
                }
                is SocketTimeoutException ->
                    return context.getString(R.string.ex_timeout)
                is IOException -> {
                    return context.getString(R.string.ex_network)
                }
                is UnknownError -> {
                    return err.message ?: ""
                }
            }
            return ""
        }
    }

}