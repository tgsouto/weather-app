package com.tiagosouto.weatherapp.view.adapter

import android.app.Activity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.squareup.picasso.Picasso
import com.tiagosouto.weatherapp.App
import com.tiagosouto.weatherapp.R
import com.tiagosouto.weatherapp.data.model.ResponseModel

class MapInfoAdapter (activity: Activity): GoogleMap.InfoWindowAdapter {

    private var appPrefs = App.getAppPrefs()
    private var list: MutableList<ResponseModel.WeatherObject> = ArrayList()
    private var mWindow: View? = null

    init {
        mWindow = activity.layoutInflater.inflate(R.layout.custom_info_window, null)
    }

    override fun getInfoContents(p0: Marker?): View? {
        return null
    }

    override fun getInfoWindow(p0: Marker?): View {
        bind(p0!!, mWindow!!)
        return mWindow!!
    }

    fun addList (list: List<ResponseModel.WeatherObject>) {
        this.list = list as MutableList<ResponseModel.WeatherObject>
    }

    private fun bind (marker: Marker, view: View) {
        val index = marker.zIndex.toInt()
        val item = list[index]

        val tempText = "${Math.round(item.main.temperature)}${appPrefs.measurementName}"
        val tempMaxText = "${Math.round(item.main.tempMax)}${appPrefs.measurementName}"
        val tempMinText = "${Math.round(item.main.tempMin)}${appPrefs.measurementName}"
        view.findViewById<TextView>(R.id.labelCityName).text = item.name
        view.findViewById<TextView>(R.id.labelDay).text = item.weather[0].description
        view.findViewById<TextView>(R.id.labelTemp).text = tempText
        view.findViewById<TextView>(R.id.labelMaxima).text = tempMaxText
        view.findViewById<TextView>(R.id.labelMinimum).text = tempMinText

        val imageIcon = view.findViewById<ImageView>(R.id.imageIcon)
        val icon = App.urlIcon + item.weather[0].icon + ".png"
        Picasso.get()
                .load(icon)
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_launcher_foreground)
                .into(imageIcon)
    }
}