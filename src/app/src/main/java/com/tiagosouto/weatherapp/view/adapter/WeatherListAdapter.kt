package com.tiagosouto.weatherapp.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.tiagosouto.weatherapp.R
import com.tiagosouto.weatherapp.data.model.ResponseModel
import com.tiagosouto.weatherapp.view.viewholder.WeatherViewHolder

class WeatherListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list: MutableList<ResponseModel.WeatherObject> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_weather, parent, false)
        return WeatherViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as WeatherViewHolder
        holder.bind(this.list[position])
    }

    fun addAll (list: List<ResponseModel.WeatherObject>) {
        this.list = list as MutableList<ResponseModel.WeatherObject>
        notifyItemRangeChanged(0, this.list.size)
    }
}