package com.tiagosouto.weatherapp.data.model

object RequestModel {
    data class Weather (
            var lat: Double,
            var lon: Double,
            var appid: String,
            var units: String
    ) {
        fun toMap () : Map<String, String> {
            val map: HashMap<String, String> = HashMap()

            map["lon"] = lon.toString()
            map["lat"] = lat.toString()
            map["appid"] = appid
            map["cnt"] = 50.toString()
            map["cluster"] = "yes"
            map["lang"] = "pt"
            map["units"] = units

            return map
        }
    }
}