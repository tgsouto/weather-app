package com.tiagosouto.weatherapp.core.util

import com.tiagosouto.weatherapp.R
import com.tiagosouto.weatherapp.data.model.RequestModel
import com.tiagosouto.weatherapp.data.model.ResponseModel

object ViewModelUtil {
    fun calcDistance (me: RequestModel.Weather, city: ResponseModel.Coordinate): Double {
        val dlon: Double = Math.toRadians(city.latitude) - Math.toRadians(me.lat)
        val dlat: Double = Math.toRadians(city.longitude) - Math.toRadians(me.lon)
        val a: Double
        val dist: Double
        a = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(dlat / 2), 2.0) + Math.cos(Math.toRadians(me.lat)) * Math.cos(Math.toRadians(city.latitude)) * Math.pow(Math.sin(dlon / 2), 2.0)))
        dist = 6371 * a
        return dist
    }

    fun getMeasurement (id: Int): String {
        return if (id == R.id.menu_celsius) {
            "metric"
        } else {
            "imperial"
        }
    }
}