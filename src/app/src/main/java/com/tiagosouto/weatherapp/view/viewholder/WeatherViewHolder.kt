package com.tiagosouto.weatherapp.view.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.tiagosouto.weatherapp.App
import com.tiagosouto.weatherapp.R
import com.tiagosouto.weatherapp.core.util.DateUtil
import com.tiagosouto.weatherapp.data.model.ResponseModel
import java.text.NumberFormat
import java.util.*

class WeatherViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var appPrefs = App.getAppPrefs()

    var labelCityName: TextView = itemView.findViewById(R.id.labelCityName)
    var labelDay: TextView = itemView.findViewById(R.id.labelDay)
    var labelTemp: TextView = itemView.findViewById(R.id.labelTemp)
    var labelMaxima: TextView = itemView.findViewById(R.id.labelMaxima)
    var labelMinimum: TextView = itemView.findViewById(R.id.labelMinimum)
    val labelDistance: TextView = itemView.findViewById(R.id.labelDistance)
    var imageIcon: ImageView = itemView.findViewById(R.id.imageIcon)

    fun bind (model: ResponseModel.WeatherObject) {
        labelCityName.text = model.name
        labelDay.text = model.weather[0].description
        val tempText = "${Math.round(model.main.temperature)}${appPrefs.measurementName}"
        labelTemp.text = tempText
        val tempMaxText = "${Math.round(model.main.tempMax)}${appPrefs.measurementName}"
        labelMaxima.text = tempMaxText
        val tempMinText = "${Math.round(model.main.tempMin)}${appPrefs.measurementName}"
        labelMinimum.text = tempMinText

        val format = NumberFormat.getNumberInstance()
        format.maximumFractionDigits = 2
        val distance = format.format(model.distance) + "km"
        labelDistance.text = distance

        val icon = App.urlIcon + model.weather[0].icon + ".png"
        Picasso.get()
                .load(icon)
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_launcher_foreground)
                .into(imageIcon)
    }
}