package com.tiagosouto.weatherapp.view.activity

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.tiagosouto.weatherapp.App
import com.tiagosouto.weatherapp.R
import com.tiagosouto.weatherapp.core.factory.WeatherViewModelFactory
import com.tiagosouto.weatherapp.core.viewmodel.WeatherViewModel
import com.tiagosouto.weatherapp.view.fragment.ListFragment
import com.tiagosouto.weatherapp.view.fragment.MapFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var fragments: Map<Int, Fragment>
    private val appPrefs = App.getAppPrefs()
    private var mMenu: Menu? = null
    lateinit var viewModel: WeatherViewModel
    var longitude: Double = 0.0
    var latitude: Double = 0.0

    init {
        val frags = HashMap<Int, Fragment>()
        frags[R.id.menu_map] = MapFragment.newInstance()
        frags[R.id.menu_list] = ListFragment.newInstance()
        this.fragments = frags
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    override fun onResume() {
        super.onResume()
        val lastFragment = this.fragments[R.id.menu_list]!!
        changeFrame(lastFragment)
        this.viewModel = ViewModelProviders.of(this, WeatherViewModelFactory(this)).get(WeatherViewModel::class.java)
        this.getLocation()

        this.viewModel.loading.observeForever {
            if (it!!) {
                loading.visibility = View.VISIBLE
            } else {
                loading.visibility = View.GONE
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menuInflater.inflate(R.menu.menu_main, menu)
        this.mMenu = menu
        this.mMenu!!.findItem(R.id.menu_list).isVisible = false
        this.mMenu!!.findItem(R.id.menu_map).isVisible = true
        if (this.appPrefs.measurement == R.id.menu_celsius) {
            this.mMenu!!.findItem(R.id.menu_farenheit).isVisible = true
            this.mMenu!!.findItem(R.id.menu_celsius).isVisible = false
        } else if (this.appPrefs.measurement == R.id.menu_farenheit) {
            this.mMenu!!.findItem(R.id.menu_farenheit).isVisible = false
            this.mMenu!!.findItem(R.id.menu_celsius).isVisible = true
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_map -> {
                this.viewModel.loading.value = true
                this.mMenu!!.findItem(R.id.menu_list).isVisible = true
                this.mMenu!!.findItem(R.id.menu_map).isVisible = false
                changeFrame(this.fragments[item.itemId]!!)
                this.loadData()
            }
            R.id.menu_list -> {
                this.viewModel.loading.value = true
                this.mMenu!!.findItem(R.id.menu_list).isVisible = false
                this.mMenu!!.findItem(R.id.menu_map).isVisible = true
                changeFrame(this.fragments[item.itemId]!!)
                this.loadData()
            }
            R.id.menu_celsius -> {
                this.appPrefs.measurement = item.itemId
                this.appPrefs.measurementName = this.resources.getString(R.string.celsius)
                this.mMenu!!.findItem(R.id.menu_farenheit).isVisible = true
                this.mMenu!!.findItem(R.id.menu_celsius).isVisible = false
                this.loadData()
            }
            R.id.menu_farenheit -> {
                this.appPrefs.measurement = item.itemId
                this.appPrefs.measurementName = this.resources.getString(R.string.farenheit)
                this.mMenu!!.findItem(R.id.menu_farenheit).isVisible = false
                this.mMenu!!.findItem(R.id.menu_celsius).isVisible = true
                this.loadData()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun changeFrame (frag: Fragment) {
        this.supportFragmentManager.inTransaction {
            replace(R.id.layoutNavigation, frag)
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLocation () {
        fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    this.longitude = location!!.longitude
                    this.latitude = location.latitude
                    loadData()
                }
    }

    private fun loadData () {
        this.viewModel.getWeather(this.latitude, this.longitude)
    }

    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
        beginTransaction().func().commitAllowingStateLoss()
    }
}
