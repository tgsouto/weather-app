package com.tiagosouto.weatherapp.data.model

import com.google.gson.annotations.SerializedName

object ResponseModel {
    data class Coordinate (
            @SerializedName("lon")
            var longitude: Double,
            @SerializedName("lat")
            var latitude: Double
    )

    data class Weather (
            var id: Int,
            var main: String,
            var description: String,
            var icon: String,
            var iconRes: Int
    )

    data class Main (
            @SerializedName("temp")
            var temperature: Double,
            var pressure: Double,
            var humidity: Double,
            @SerializedName("temp_min")
            var tempMin: Double,
            @SerializedName("temp_max")
            var tempMax: Double
    )

    data class Wind (
            var speed: Double,
            @SerializedName("deg")
            var degrees: Double
    )

    data class Clouds (
            var all: Int
    )

    data class Rain (
            var threeHours: Double
    )

    data class Snow (
            var threeHours: Double
    )

    data class WeatherObject (
            var id: Int,
            var name: String,
            var distance: Double,
            var coord: Coordinate,
            var main: Main,
            var dt: Long,
            var wind: Wind,
            var rain: Rain?,
            var snow: Snow?,
            var clouds: Clouds?,
            var weather: List<Weather>
    )

    data class WeatherResponse (
            var cod: Int,
            var count: Int,
            var message: String,
            var list: List<WeatherObject>
    )
}