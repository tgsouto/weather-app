package com.tiagosouto.weatherapp.core.factory

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.tiagosouto.weatherapp.core.viewmodel.WeatherViewModel
import com.tiagosouto.weatherapp.data.remote.API
import com.tiagosouto.weatherapp.data.repository.WeatherRepository

class WeatherViewModelFactory(
        private val context: Context
): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(WeatherViewModel::class.java)){
            val weatherRepository = WeatherRepository(API.create())
            return WeatherViewModel(context, weatherRepository) as T
        }
        throw ClassNotFoundException(WeatherViewModel::class.java.simpleName + " not found")
    }
}