package com.tiagosouto.weatherapp.core.util

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build

/**
 * Created by tiagosouto on 27/09/17.
 */
object PermissionUtil {
    val ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION"

    fun hasPermission (context: Context, permission: String) : Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            (context.checkSelfPermission(permission)== PackageManager.PERMISSION_GRANTED)
        } else {
            return true
        }
    }
}