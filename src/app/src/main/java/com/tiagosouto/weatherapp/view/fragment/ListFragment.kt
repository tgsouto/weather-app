package com.tiagosouto.weatherapp.view.fragment


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tiagosouto.weatherapp.R
import com.tiagosouto.weatherapp.core.factory.WeatherViewModelFactory
import com.tiagosouto.weatherapp.core.viewmodel.WeatherViewModel
import com.tiagosouto.weatherapp.view.activity.MainActivity
import com.tiagosouto.weatherapp.view.adapter.WeatherListAdapter

/**
 * A simple [Fragment] subclass.
 * Use the [ListFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ListFragment : Fragment() {

    private var mView: View? = null
    private lateinit var adapter: WeatherListAdapter
    private lateinit var act: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.act = this.activity!! as MainActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        this.mView = inflater.inflate(R.layout.fragment_list, container, false)
        this.init()
        return this.mView
    }

    override fun onResume() {
        super.onResume()

        this.act.viewModel.getWeather().observe(this, Observer {
            if (it != null && !it.isEmpty()) {
                this.adapter.addAll(it)
            }
        })
    }

    private fun init () {
        this.adapter = WeatherListAdapter()
        val listWeather: RecyclerView = this.mView!!.findViewById(R.id.listCity)
        listWeather.adapter = this.adapter
//        listWeather.setHasFixedSize(true)
        listWeather.layoutManager = LinearLayoutManager(this.activity)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ListFragment.
         */
        @JvmStatic
        fun newInstance() = ListFragment()
    }
}
