package com.tiagosouto.weatherapp.view.activity

import android.os.Build
import android.os.Bundle
import com.tiagosouto.weatherapp.core.util.PermissionUtil


class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PermissionUtil.hasPermission(applicationContext, PermissionUtil.ACCESS_FINE_LOCATION)) {
                this.redirToMain()
            } else {
                this.redirToRequestPermission()
            }
        } else {
            this.redirToMain()
        }
    }
}
