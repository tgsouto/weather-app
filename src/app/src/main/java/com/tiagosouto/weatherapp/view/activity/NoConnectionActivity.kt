package com.tiagosouto.weatherapp.view.activity

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.tiagosouto.weatherapp.R
import kotlinx.android.synthetic.main.activity_no_connection.*

class NoConnectionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_no_connection)

        btnTryAgain.setOnClickListener {
            loading.visibility = View.VISIBLE
            btnTryAgain.visibility = View.GONE
            if (this.hasConnection()) {
                this.redirToSplash()
            } else {
                loading.visibility = View.GONE
                btnTryAgain.visibility = View.VISIBLE
            }
        }
    }

    private fun hasConnection (): Boolean {
        val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    private fun redirToSplash () {
        val intent = Intent(applicationContext, SplashActivity::class.java)
        this.startActivity(intent)
        finish()
    }
}
