package com.tiagosouto.weatherapp

import android.support.test.runner.AndroidJUnit4
import com.tiagosouto.weatherapp.data.model.RequestModel
import com.tiagosouto.weatherapp.data.remote.API
import com.tiagosouto.weatherapp.data.repository.WeatherRepository
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RequestFromApiTest {
    var weatherRepository = WeatherRepository(API.create())

    @Test
    fun getDataFromLocation () {
        try {
            val weatherModel = RequestModel.Weather(-23.1723878, -46.9085983, App.apiId, "metric")
            val result = weatherRepository.getWeather(weatherModel).blockingFirst()
            Assert.assertTrue("Require data from API.", result.list.isNotEmpty())
        }catch (e: Exception){
            Assert.fail("Check internet connection.")
        }
    }

    @Test
    fun getDataChangeToFarenheit () {
        try {
            val weatherModel = RequestModel.Weather(-23.1723878, -46.9085983, App.apiId, "imperial")
            val result = weatherRepository.getWeather(weatherModel).blockingFirst()
            Assert.assertTrue("Require data from API.", result.list.isNotEmpty())
        }catch (e: Exception){
            Assert.fail("Check internet connection.")
        }
    }
}